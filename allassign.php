<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <title>Dashboard</title>
        <style>
            .navbar{
                background-color: #212f42 !important;
            }
        </style>
    </head>

    <body>
        <!--=======================Start of Navbar=====================-->
        <nav class="navbar navbar-expand-md bg-light navbar-light fixed-top">
            <div class="container">
                <a href="#"><button class="navbar-brand btn btn-danger"
                   data-toggle="modal" data-target="#exampleModalLong">
                    <span class="text-white"><b>Date&Time Assignment</b></span></button></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
                </div>
            </div>
        </nav>
        <div class="container pt-4 mt-5">
            <h4 style="font-family: Trebuchet MS">Solution</h4>
            <?php 
               $d1 = 1532163194; //date(format, timestamp) ...i.e this is the current time..
               $d2 = 1536421369;
               $d1_value = date('jS M, Y h:i A',  $d1);
               $d2_value = date('jS M, Y h:i A',  $d2);
              
               $d1_date = date('Y-m-d', $d1);
               $d2_date = date('Y-m-d', $d2);

               $date1=date_create($d1_date);
               $date2=date_create($d2_date);
               $diff=date_diff($date1,$date2);
               $diff->format("%R%a days");
              
               //echo date('jS M, Y h:1 A', time()); not picking the right time ...
            ?>
            <p>1i) 1532163194 datetime conversion is: <b><?php echo $d1_value ?> </b> </p>
            <p> ii) 1536421369 datetime conversion is: <b> <?php echo $d2_value ?> </b></p> 
            <p> iii) The difference in days is: <b> <?php echo $diff->format("%a days"); ?></b></p>
    

            <p> 2) Digit 4 converted to month:  <?php echo date("F", mktime(0,0,0,4,20));?></p>
            <p> 3)</p>
            <div class="card mt-4">
                <div class="card-header">MonthLooping</div>
                <div class="card-body">
                     <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                         <input type="month" name="startmonth" class="form-control" placeholder="enter start month and year" required><br>
                         <input type="month" name="endmonth" class="form-control"  placeholder="enter end month and year" required> 
                         <button type="sumbit" name="submit" class="btn btn-info mt-3">Submit</button>
                     </form>
                <?php if(isset($_POST['submit'])){
                     $startmonth = $_POST['startmonth'];
                     $endmonth = $_POST['endmonth'];
                     $start = strtotime($startmonth);
                     $end = strtotime($endmonth);
                     $startmonth1 = date('Y', $start).date('m', $start);
                     $endmonth2 = date('Y', $end).date('m', $end);
                     while($start <= $end){?>
                        <div class="ans ml-2 mb-2">
                            <b><?php 
                                echo date('F Y', $start) ."<br>";
                                $start = strtotime("+1month", $start);
                                //$startmonth1 = date('Y', $start).date('m', $start);
                                 ?>   
                                </b>
                        </div>
                     
                <?php } }?>
                </div>
            </div>
            <p> 4)</p>
            <div class="card mt-4">
                <div class="card-header">DateLooping</div>
                <div class="card-body">
                     <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                         <input type="date" name="startdate" class="form-control" placeholder="enter start date a" required><br>
                         <input type="date" name="enddate" class="form-control"  placeholder="enter end month year"required> 
                         <button type="sumbit" name="submit1" class="btn btn-info mt-3">Submit</button>
                     </form>
                </div>
                <?php if(isset($_POST['submit1'])){
                     $startdate = $_POST['startdate'];
                     $enddate = $_POST['enddate'];

                     $start  = strtotime($startdate);
                     $end = strtotime($enddate);
                     while($start <= $end){ ?>
                        <div class="answers ml-3 mb-2">
                            <b><?php echo date('jS M, Y', $start) . "<br>";
                            $start = strtotime("+1 day", $start); 
                            //$starter = date('Y', $start).date('m', $start).date('d', $start); ?></b>
                        </div>   
               <?php } }?>
            </div>
            <p> 5) </p>
            <div class="card mt-4">
                <div class="card-header">
                <h5 style="font-family: cursive;">DATE/TIME REFERENCE</h5>   
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped custom-table datatable mb-0">
                            <thead>
                            <tr>
                                <th>PARAMETER</th>
                                <th>DEFINITION</th>
                                <th>EXAMPLE</th> 
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Formats</td>
                                    <td>Required. Specifies the format of the outputted date string. The following characters can be used: </td>
                                    <td>Script-Outputs</td>
                                </tr>
                                <tr>
                                    <td>d</td>
                                    <td>The day of the month(from 01 to 31)</td>
                                    <td><?php echo date('d') ?></td>
                                </tr>
                                <tr>
                                    <td>D</td>
                                    <td> A textual representation of a day (three letters)</td>
                                    <td><?php echo date('D') ?></td>
                                </tr>
                                <tr>
                                    <td>j</td>
                                    <td> The day of the month without leading zeros (1 to 31)</td>
                                    <td><?php echo date('j') ?></td>
                                </tr>
                                <tr>
                                    <td>l</td>
                                    <td> A full textual representation of a day</td>
                                    <td><?php echo date('l') ?></td>
                                </tr>
                                <tr>
                                    <td>N</td>
                                    <td>The ISO-8601 numeric representation of a day (1 for Monday, 7 for Sunday)</td>
                                    <td><?php echo date('N') ?></td>
                                </tr>
                                <tr>
                                    <td>S</td>
                                    <td>The English ordinal suffix for the day of the month (2 characters st, nd, rd or th. Works well with j)</td>
                                    <td><?php echo date('S') ?></td>
                                </tr>
                                <tr>
                                    <td>w</td>
                                    <td>A numeric representation of the day (0 for Sunday, 6 for Saturday)</td>
                                    <td><?php echo date('w') ?></td>
                                </tr>
                                <tr>
                                    <td>z</td>
                                    <td>The day of the year (from 0 through 365)</td>
                                    <td><?php echo date('z') ?></td>
                                </tr>
                                <tr>
                                    <td>W</td>
                                    <td>The ISO-8601 week number of year (weeks starting on Monday)</td>
                                    <td><?php echo date('W') ?>/td>
                                </tr>
                                <tr>
                                    <td>F</td>
                                    <td>A full textual representation of a month (January through December)</td>
                                    <td><?php echo date('F') ?></td>
                                </tr>
                                <tr>
                                    <td>m</td>
                                    <td>A numeric representation of a month (from 01 to 12)</td>
                                    <td><?php echo date('m') ?></td>
                                </tr>
                                <tr>
                                    <td>M</td>
                                    <td>A short textual representation of a month (three letters)</td>
                                    <td><?php echo date('M') ?></td>
                                </tr>
                                <tr>
                                    <td>n</td>
                                    <td>A numeric representation of a month, without leading zeros (1 to 12)</td>
                                    <td><?php echo date('n') ?></td>
                                </tr>
                                <tr>
                                    <td>t</td>
                                    <td>The number of days in the given month</td>
                                    <td><?php echo date('t') ?></td>
                                </tr>
                                <tr>
                                    <td>L</td>
                                    <td>Whether it's a leap year (1 if it is a leap year, 0 otherwise)</td>
                                    <td<?php echo date('L') ?></td>
                                </tr>
                                <tr>
                                    <td>o</td>
                                    <td>The ISO-8601 year number</td>
                                    <td><?php echo date('o') ?></td>
                                </tr>
                                <tr>
                                    <td>Y</td>
                                    <td>A four digit representation of a year</td>
                                    <td><?php echo date('Y') ?></td>
                                </tr>
                                <tr>
                                    <td>y</td>
                                    <td> A two digit representation of a year</td>
                                    <td><?php echo date('y') ?></td>
                                </tr>
                                <tr>
                                    <td>a</td>
                                    <td>Lowercase am or pm</td>
                                    <td><?php echo date('a') ?></td>
                                </tr>
                                <tr>
                                    <td>A</td>
                                    <td>Uppercase AM or PM</td>
                                    <td><?php echo date('A') ?></td>
                                </tr>
                                <tr>
                                    <td>B</td>
                                    <td>Swatch Internet time (000 to 999)</td>
                                    <td><?php echo date('B') ?></td>
                                </tr>
                                <tr>
                                    <td>g</td>
                                    <td>12-hour format of an hour (1 to 12)</td>
                                    <td><?php echo date('g') ?></td>
                                </tr>
                                <tr>
                                    <td>G</td>
                                    <td>24-hour format of an hour (0 to 23)</td>
                                    <td><?php echo date('G') ?></td>
                                </tr>
                                <tr>
                                    <td>h</td>
                                    <td>12-hour format of an hour (01 to 12)</td>
                                    <td><?php echo date('h') ?></td>
                                </tr>
                                <tr>
                                    <td>H</td>
                                    <td>24-hour format of an hour (00 to 23)</td>
                                    <td><?php echo date('H') ?></td>
                                </tr>
                                <tr>
                                    <td>i</td>
                                    <td>Minutes with leading zeros (00 to 59)</td>
                                    <td><?php echo date('i') ?></td>
                                </tr>
                                <tr>
                                    <td>s</td>
                                    <td>Seconds, with leading zeros (00 to 59)</td>
                                    <td><?php echo date('s') ?></td>
                                </tr>
                                <tr>
                                    <td>u</td>
                                    <td>Microseconds (added in PHP 5.2.2)</td>
                                    <td><?php echo date('u') ?></td>
                                </tr>
                                <tr>
                                    <td>e</td>
                                    <td>The timezone identifier (Examples: UTC, GMT, Atlantic/Azores)/td>
                                    <td><?php echo date('e') ?></td>
                                </tr>
                                <tr>
                                    <td>I</td>
                                    <td>Whether the date is in daylights savings time (1 if Daylight Savings Time, 0 otherwise)
                                    <td><?php echo date('I') ?></td>
                                </tr>
                                <tr>
                                    <td>O</td>
                                    <td>Difference to Greenwich time (GMT) in hours (Example: +0100)
                                    <td><?php echo date('O') ?></td>
                                </tr>
                                <tr>
                                    <td>P</td>
                                    <td>Difference to Greenwich time (GMT) in hours:minutes (added in PHP 5.1.3)
                                    <td><?php echo date('P') ?></td>
                                </tr>
                                <tr>
                                    <td>T</td>
                                    <td>Timezone abbreviations (Examples: EST, MDT)
                                    <td><?php echo date('T') ?></td>
                                </tr>
                                <tr>
                                    <td>Z</td>
                                    <td>Timezone offset in seconds. The offset for timezones west of UTC is negative (-43200 to 50400)
                                    <td><?php echo date('Z') ?></td>
                                </tr>
                                <tr>
                                    <td>r</td>
                                    <td>The RFC 2822 formatted date (e.g. Fri, 12 Apr 2013 12:01:05 +0200)
                                    <td><?php echo date('r') ?></td>
                                </tr>
                                <tr>
                                    <td>c</td>
                                    <td>The ISO-8601 date (e.g. 2013-05-05T16:34:42+00:00)
                                    <td><?php echo date('c') ?></td>
                                </tr>
                                <tr>
                                    <td>U</td>
                                    <td>The seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
                                    <td><?php echo date('U') ?></td>
                                </tr>
                            </tbody> 
                        </table>
                    </div>
                </div>
            </div>


        </div>



     <script src="js/jquery.min.js"></script>
     <script src="js/bootstrap.bundle.js"></script>
     <script src="js/bootstrap.bundle.min.js"></script>
     <script src="js/fontawesome-all.js"></script>
     <script src="js/bootstrap.min.js"></script>
    </body>
</html>